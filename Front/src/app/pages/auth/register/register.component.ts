import { Component, inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  
  form: FormGroup;
  authService = inject(AuthService)

  constructor(){
    this.form = new FormGroup({
      userName: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
      confirmPassword: new FormControl()
    })
  }

  async onSubmit(){
    try {
      const respone = await this.authService.register(this.form.value)
      console.log(respone)
    } catch (error) {     
      console.log('error: ',error) 
    }
    
  }
}
