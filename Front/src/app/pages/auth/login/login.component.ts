import { Component, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { Router, RouterLink, RouterLinkActive } from '@angular/router';
import { LoginResponse } from '../../../core/models/login-response';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    ReactiveFormsModule, 
    RouterLink, 
    RouterLinkActive, 
    MatCardModule, 
    MatInputModule, 
    MatIconModule, 
    MatFormFieldModule, 
    MatButtonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  form: FormGroup;
  error: any;
  authService = inject(AuthService)
  router = inject(Router)
  formBuilder = inject(FormBuilder)
  

  constructor(){
    this.form =this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['',Validators.required]
    })
    this.error = ''
  }

  async onSubmit(){
    try {
      const response = await this.authService.login(this.form.value)      
      if(response.isSuccess){
        this.saveUserDate(response.result!)
        this.router.navigate(['tasks'])
      } else {
        this.error = response.message || 'Ocurrio un error';
      }
    } catch (error) {
      this.error = error || 'Ocurrio un error';
      console.log('error catch: ',error)
    }   
  }

  saveUserDate(data:LoginResponse){
    localStorage.setItem('accessToken', data.accessToken!)
  }
}
