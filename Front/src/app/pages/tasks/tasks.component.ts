import { Component } from '@angular/core';
import { TasksListComponent } from '../../shared/components/tasks-list/tasks-list.component';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIcon} from '@angular/material/icon'
@Component({
  selector: 'app-tasks',
  standalone: true,
  imports: [TasksListComponent, MatCardModule, MatButtonModule,MatIcon],
  templateUrl: './tasks.component.html',
  styleUrl: './tasks.component.css'
})
export class TasksComponent {

}
