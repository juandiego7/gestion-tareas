import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Task } from '../models/task';
import { ResponseApi } from '../models/response-api';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  private httpClient = inject(HttpClient)
  private baseUrl: string;  

  constructor() { 
    this.baseUrl = 'http://localhost:5136/api/v1/Tasks'
  }

  getTasks(){
    return firstValueFrom(
      this.httpClient.get<ResponseApi<Task[]>>(`${this.baseUrl}`)
    )
  }

}
