import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { LoginResponse } from '../models/login-response';
import { LoginRequest } from '../models/login-request';
import { RegisterUser } from '../models/register-user';
import { ResponseApi } from '../models/response-api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private httpClient = inject(HttpClient)
  private baseUrl: string;  

  constructor() { 
    this.baseUrl = 'http://localhost:5136/api/v1/Auth'
  }

  register(formValue: RegisterUser){
    return firstValueFrom(
      this.httpClient.post<any>(`${this.baseUrl}/register`, formValue)
    )
  }

  login(formValue: LoginRequest){
    return firstValueFrom(
      this.httpClient.post<ResponseApi<LoginResponse>>(`${this.baseUrl}/login`, formValue)
    )
  }

  logout(){
    localStorage.removeItem('accessToken');
  }

  isLogged(): boolean{
    const token = localStorage.getItem('accessToken');
    return token ? true : false;
  }

  getToken(){
    return localStorage.getItem('accessToken');
  }
}
