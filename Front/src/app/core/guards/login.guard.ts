import { inject } from "@angular/core";
import { Router } from "@angular/router";

export const loginGuard = () => {

    const router = inject(Router)

    const token = localStorage.getItem('accessToken');
    if (token) {
        return true;
    }
    router.navigate(['/login']);
    return false;
}