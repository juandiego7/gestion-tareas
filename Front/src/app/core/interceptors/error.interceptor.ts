import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { catchError, throwError } from 'rxjs';
export const errorInterceptor: HttpInterceptorFn = (req, next) => {

  return next(req).pipe(catchError((error: HttpErrorResponse) => {
    let errorMessage = "";
    console.log(error);
    
    try {
      if (error.error.errors) {
        errorMessage = Object.values(error.error.errors)
          .flatMap(messages => messages)
          .join('\n');
      } else {
        errorMessage = error.error.Message|| error.error.message  || error.message;
      }
    } catch {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(() => new Error(errorMessage));
  }));
};
