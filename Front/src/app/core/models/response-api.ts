export interface ResponseApi<T> {
    isSuccess: boolean;
    message?: string;
    code?: string;
    result?: T;
  }
  