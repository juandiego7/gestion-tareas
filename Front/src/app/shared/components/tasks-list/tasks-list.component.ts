import { Component, OnInit, inject } from '@angular/core';
import { TasksService } from '../../../core/services/tasks.service';
import { Task } from '../../../core/models/task';
import { MatTableModule } from '@angular/material/table'

@Component({
  selector: 'app-tasks-list',
  standalone: true,
  imports: [MatTableModule],
  templateUrl: './tasks-list.component.html',
  styleUrl: './tasks-list.component.css'
})
export class TasksListComponent implements OnInit{
  
  tasksService = inject(TasksService)
  tasks: Task[] = []
  displayedColumns: string[] = ['title', 'description', 'dueDate', 'isCompleted', 'actions']

  async ngOnInit(){
   this.getTasks()
  }

  async getTasks() {
    try {
      const response = await this.tasksService.getTasks()      
      console.log(response)
      if(response.isSuccess){
        this.tasks = response.result!
      }
    } catch (error) {
      console.log('error: ',error)
    }  
  }

}
