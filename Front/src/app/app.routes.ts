import { Routes } from '@angular/router';
import { loginGuard } from './core/guards/login.guard';

export const routes: Routes = [
    {
        path: 'login',
        loadComponent: () => import('./pages/auth/login/login.component').then(m => m.LoginComponent)
    },
    {
        path: 'register',
        loadComponent: () => import('./pages/auth/register/register.component').then(m => m.RegisterComponent)
    },
    {
        path: '',
        redirectTo: 'tasks',
        pathMatch: 'full'   
    },
    {
        path: 'tasks',
        loadComponent: () => import('./shared/layout/main-layout/main-layout.component').then(m => m.MainLayoutComponent),
        canActivate: [loginGuard],
        children: [
            {
                path: '',
                loadComponent: () => import('./pages/tasks/tasks.component').then(m => m.TasksComponent)
            }
        ]
    }
    
];
