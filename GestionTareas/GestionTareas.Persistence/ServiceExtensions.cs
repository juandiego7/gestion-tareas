﻿using GestionTareas.Application.Interfaces.Repositories;
using GestionTareas.Persistence.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace GestionTareas.Persistence
{
    public static class ServiceExtensions
    {
        public static void AddPersistence(this IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<ITaskRepository, TaskRepository>();
        }
    }
}
