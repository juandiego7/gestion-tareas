﻿using GestionTareas.Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GestionTareas.Persistence.Context
{
    public class PersistenceContext : IdentityDbContext
    {
        public PersistenceContext(DbContextOptions<PersistenceContext> options) : base(options) 
        { 
        }

        public DbSet<TaskEntity> Tasks { get; set; }

    }
}
//dotnet ef migrations add InitialCreate --startup-project ..\GestionTareas.API
// dotnet ef database update --startup-project ..\GestionTareas.API

