﻿using Ardalis.Specification.EntityFrameworkCore;
using GestionTareas.Application.Interfaces.Repositories;
using GestionTareas.Persistence.Context;

namespace GestionTareas.Persistence.Repositories
{
    public class Repository<T> : RepositoryBase<T>, IRepository<T> where T : class 
    {
        public Repository(PersistenceContext dbContext) : base(dbContext) 
        {

        }
    }
}
