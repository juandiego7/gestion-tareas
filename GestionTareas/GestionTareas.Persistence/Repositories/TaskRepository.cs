﻿using GestionTareas.Application.Interfaces.Repositories;
using GestionTareas.Domain.Entities;
using GestionTareas.Persistence.Context;

namespace GestionTareas.Persistence.Repositories
{
    public class TaskRepository : Repository<TaskEntity>, ITaskRepository
    {
        public TaskRepository(PersistenceContext dbContext) : base(dbContext)
        {
        }
    }
   
}
