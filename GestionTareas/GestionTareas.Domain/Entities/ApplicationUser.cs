﻿using Microsoft.AspNetCore.Identity;

namespace GestionTareas.Domain.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() {
        }
        public ICollection<TaskEntity> Tasks { get; set; }
    }
}
