﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;
using GestionTareas.Application.Exceptions;
using GestionTareas.Application.Wrappers;

namespace GestionTareas.Api.Middlewares
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                var responseModel = new Response<string>() { IsSuccess = false, Message = (error?.Message + " " + error?.InnerException?.Message).Trim() };
                switch (error)
                {
                    case NotFoundException:
                        response.StatusCode = (int)HttpStatusCode.NotFound;
                        break;
                    case BadRequestException:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        break;
                    case UnauthorizedException:
                        response.StatusCode = (int)HttpStatusCode.Unauthorized;
                        break;
                    // case ValidationException validationException:
                    //     Console.WriteLine("validationException");
                    //     response.StatusCode = (int)HttpStatusCode.BadRequest;
                    //     responseModel.Message = validationException.Message;
                    //     break;
                    default:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        break;
                }

                var result = JsonSerializer.Serialize(responseModel);
                await response.WriteAsync(result);
            }
        }
    }
}

