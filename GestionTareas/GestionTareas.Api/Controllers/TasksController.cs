﻿using System.Security.Claims;
using GestionTareas.Application.DTOs;
using GestionTareas.Application.Interfaces.Services;
using GestionTareas.Application.Wrappers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GestionTareas.Api.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _service;
        public TasksController(ITaskService service) { 
            _service = service;
        }

        [HttpGet]
        public async Task<Response<IEnumerable<TaskDto>>> GetTasksByUser()
        {
            Console.WriteLine("GetTasksByUser: ");
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            Console.WriteLine("userId: "+userId);
            return await _service.GetTasksByUserAsync(userId!);
        }

        [HttpPost]
        public async Task<Response<TaskDto>> AddTask(TaskDto task)
        {
            return await _service.Add(task);
        }

        [HttpPut]
        public async Task<Response<bool>> UpdateTask(TaskDto task)
        {
            return await _service.Update(task);
        }

        [HttpDelete("{id}")]
        public async Task<Response<bool>> DeleteTask(int id)
        {
            return await _service.Delete(id); 
        }

    }
}
