using GestionTareas.Application.DTOs;
using GestionTareas.Application.Interfaces.Services;
using GestionTareas.Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace GestionTareas.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _service;

        public AuthController(IAuthService service)
        {
            _service = service;
        }

        [HttpPost("register")]
        public async Task<Response<bool>> Regsiter(RegisterUserDto user)
        {
            return await _service.RegisterUser(user);
        }

        [HttpPost("login")]
        public async Task<Response<LoginResponseDto>> Login(LoginUserDto user)
        {
            return await _service.Login(user);
        }

    }
}