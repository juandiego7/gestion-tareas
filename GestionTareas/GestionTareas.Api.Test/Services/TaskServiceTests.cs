﻿using AutoMapper;
using GestionTareas.Application.DTOs;
using GestionTareas.Application.Exceptions;
using GestionTareas.Application.Filters;
using GestionTareas.Application.Interfaces.Repositories;
using GestionTareas.Application.Mappings;
using GestionTareas.Application.Services;
using GestionTareas.Domain.Entities;
using Moq;

namespace GestionTareas.Api.Test.Services
{
    public class TaskServiceTests
    {
        private readonly Mock<ITaskRepository> _repositoryMock;
        private readonly IMapper _mapper;
        private readonly TaskService _taskService;

        public TaskServiceTests()
        {
            _repositoryMock = new Mock<ITaskRepository>();
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<GeneralProfile>()));
            _taskService = new TaskService(_repositoryMock.Object, _mapper);
        }

        [Fact]
        public async Task Add_ShouldReturnTaskDto_WhenTaskIsAdded()
        {
            // Arrange
            var taskDto = new TaskDto { Id = 0, Title = "Test Task" };
            var taskEntity = new TaskEntity { Id = 1, Title = "Test Task" };
            _repositoryMock.Setup(repo => repo.AddAsync(It.IsAny<TaskEntity>(), It.IsAny<CancellationToken>())).ReturnsAsync(taskEntity);

            // Act
            var response = await _taskService.Add(taskDto);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccess);
            Assert.NotNull(response.Result);
            Assert.True(response.Result.Id > 0);
            Assert.Equal(taskDto.Title, response.Result.Title);
        }

        [Fact]
        public async Task Delete_ShouldReturnTrue_WhenTaskExists()
        {
            // Arrange
            var taskEntity = new TaskEntity { Id = 1, Title = "Test Task" };
            _repositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync(taskEntity);
            _repositoryMock.Setup(repo => repo.DeleteAsync(It.IsAny<TaskEntity>(), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            // Act
            var response = await _taskService.Delete(1);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccess);
            Assert.True(response.Result);
        }

        [Fact]
        public async Task Delete_ShouldThrowException_WhenTaskDoesNotExist()
        {
            // Arrange
            _repositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync((TaskEntity)null);
            int taskId = 1; 

            // Act & Assert
            await Assert.ThrowsAsync<TaskNotFoundException>(() => _taskService.Delete(taskId));
        }

        [Fact]
        public async Task GetTasksByUserAsync_ShouldReturnTaskDtos_WhenTasksExist()
        {
            // Arrange
            var tasks = new List<TaskEntity> {
                new TaskEntity { Id = 1, Title = "Test Task 1", UserId = "123456" },
                new TaskEntity { Id = 2, Title = "Test Task 2", UserId = "123456" }
            };
            _repositoryMock.Setup(repo => repo.ListAsync(It.IsAny<TasksByUserFilter>(), It.IsAny<CancellationToken>())).ReturnsAsync(tasks);

            // Act
            string userId = "123456";
            var response = await _taskService.GetTasksByUserAsync(userId);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccess);
            Assert.NotNull(response.Result);
            Assert.Equal(2, response.Result.Count());
        }

        [Fact]
        public async Task Update_ShouldReturnTrue_WhenTaskIsUpdated()
        {
            // Arrange
            var taskDto = new TaskDto { Id = 1, Title = "Updated Task", Description = "Updated Description" };
            var taskEntity = new TaskEntity { Id = 1, Title = "Updated Task", Description = "Updated Description" };
            _repositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<TaskEntity>(), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask);

            // Act
            var response = await _taskService.Update(taskDto);

            // Assert
            Assert.NotNull(response);
            Assert.True(response.IsSuccess);
            Assert.True(response.Result);
        }

    }
}