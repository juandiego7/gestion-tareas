﻿using GestionTareas.Application.Interfaces.Repositories;
using GestionTareas.Domain.Entities;
using GestionTareas.Persistence.Context;
using GestionTareas.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;

namespace GestionTareas.Api.Test.Repositories
{
    public class TaskRepositoryTests: IDisposable
    {
        private readonly DbContextOptions<PersistenceContext> _dbContextOptions;
        private readonly PersistenceContext _context;
        private readonly ITaskRepository _repository;

        public TaskRepositoryTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<PersistenceContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            _context = new PersistenceContext(_dbContextOptions);
            _repository = new TaskRepository(_context);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public async Task AddAsync_ShouldAddTask()
        {
            // Arrange
            var task = new TaskEntity { Id = 1, Title = "Test Task", Description = "Test Description", UserId = "test-user" };

            // Act
            await _repository.AddAsync(task);

            // Assert
            var addedTask = await _context.Tasks.FindAsync(1);
            Assert.NotNull(addedTask);
            Assert.True(addedTask.Id > 0);
            Assert.Equal("Test Task", addedTask.Title);
        }

        [Fact]
        public async Task GetByIdAsync_ShouldReturnTask()
        {
            // Arrange
            var task = new TaskEntity { Id = 2, Title = "Test Task 2", Description = "Test Description 2", UserId = "test-user" };
            _context.Tasks.Add(task);

            // Act
            var result = await _repository.GetByIdAsync(2);

            // Assert
            Assert.NotNull(result);
            Assert.Equal("Test Task 2", result.Title);
        }

        [Fact]
        public async Task DeleteAsync_ShouldRemoveTask()
        {
            // Arrange
            var task = new TaskEntity { Id = 3, Title = "Test Task 3", Description = "Test Description 3", UserId = "test-user" };
            _context.Tasks.Add(task);

            // Act
            await _repository.DeleteAsync(task);

            // Assert
            var deletedTask = await _context.Tasks.FindAsync(3);
            Assert.Null(deletedTask);
        }

        [Fact]
        public async Task ListAsync_ShouldReturnAllTasks()
        {
            // Arrange
            var tasks = new List<TaskEntity>
        {
            new TaskEntity { Id = 4, Title = "Test Task 4", Description = "Test Description 4", UserId = "test-user" },
            new TaskEntity { Id = 5, Title = "Test Task 5", Description = "Test Description 5", UserId = "test-user" }
        };
            await _context.Tasks.AddRangeAsync(tasks);
            await _context.SaveChangesAsync();

            // Act
            var result = await _repository.ListAsync();

            // Assert
            Assert.Equal(2, result.Count());
        }
    }
}
