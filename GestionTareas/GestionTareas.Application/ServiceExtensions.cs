﻿using GestionTareas.Application.Interfaces.Services;
using GestionTareas.Application.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace GestionTareas.Application
{
    public static class ServiceExtensions
    {
        public static void AddApplication(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddTransient<ITaskService, TaskService>();
            services.AddTransient<IAuthService, AuthService>();
        }
    }
}
