using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionTareas.Application.Exceptions
{
    public class UserUnauthorizedException : UnauthorizedException
    {
        public UserUnauthorizedException(string message) : base(message)
        {
        }
    }
}