﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTareas.Application.Exceptions
{
    public class TaskNotFoundException : NotFoundException
    {
        public TaskNotFoundException(string id) : base($"La tarea con el ID: {id} no fue encontrado")
        {
        }
    }
}
