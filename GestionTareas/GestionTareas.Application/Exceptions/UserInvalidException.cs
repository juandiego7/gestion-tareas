using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionTareas.Application.Exceptions
{
    public class UserInvalidException: BadRequestException
    {
        public UserInvalidException(string message) : base(message)
        {
        }
    }
}