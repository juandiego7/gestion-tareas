﻿using AutoMapper;
using GestionTareas.Application.DTOs;
using GestionTareas.Application.Exceptions;
using GestionTareas.Application.Filters;
using GestionTareas.Application.Interfaces.Repositories;
using GestionTareas.Application.Interfaces.Services;
using GestionTareas.Application.Wrappers;
using GestionTareas.Domain.Entities;

namespace GestionTareas.Application.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _repository;
        private readonly IMapper _mapper;
        public TaskService(ITaskRepository repository,IMapper mapper) {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<Response<TaskDto>> Add(TaskDto task)
        {
            var taskEntity = _mapper.Map<TaskEntity>(task);
            var result = await _repository.AddAsync(taskEntity);
            var taskDto = _mapper.Map<TaskDto>(result);
            return new Response<TaskDto>(taskDto);
        }

        public async Task<Response<bool>> Delete(int id)
        {
            var task = await _repository.GetByIdAsync(id);
            if (task != null) { 
                await _repository.DeleteAsync(task);
                return new Response<bool>(true);
            }
            throw new TaskNotFoundException(id.ToString());       
        }

        public async Task<Response<IEnumerable<TaskDto>>> GetTasksByUserAsync(string userId)
        {
            if(userId is null){
                throw new UserInvalidException("userId no puede ser vacio");
            }        
            var filter = new TasksByUserFilter(userId);
            var tasks = await _repository.ListAsync(filter);
            var tasksDto = _mapper.Map<IEnumerable<TaskDto>>(tasks);
            return new Response<IEnumerable<TaskDto>>(tasksDto);
        }

        public async Task<Response<bool>> Update(TaskDto task)
        {
            var taskEntity = _mapper.Map<TaskEntity>(task);
            await _repository.UpdateAsync(taskEntity);
            return new Response<bool>(true);
        }
    }
}
