using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using GestionTareas.Application.DTOs;
using GestionTareas.Application.Exceptions;
using GestionTareas.Application.Interfaces.Services;
using GestionTareas.Application.Wrappers;
using GestionTareas.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace GestionTareas.Application.Services
{
    public class AuthService : IAuthService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public AuthService(UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            _userManager = userManager;
            _configuration = configuration;
        }

        public string GenerateToken(ApplicationUser loginUser)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]!));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, loginUser.Id),
                new Claim(ClaimTypes.Name, loginUser.UserName!),
                new Claim(ClaimTypes.Email, loginUser.Email!)
            };
            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],                
                claims: claims,
                expires: new DateTimeOffset(DateTime.Now.AddHours(-5).AddDays(1)).DateTime,
                signingCredentials: credentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<Response<LoginResponseDto>> Login(LoginUserDto loginUser)
        {
            var errorMessage = "Correo o contraseña incorrecto";
            
            var applicationUser = await _userManager.FindByEmailAsync(loginUser.Email!);
            if (applicationUser == null)
            {
                throw new UserUnauthorizedException(errorMessage);
            }

            var checkUserPasword = await _userManager.CheckPasswordAsync(applicationUser, loginUser.Password!);
            if (!checkUserPasword)
            {
                throw new UserUnauthorizedException(errorMessage);
            }                
            string token = GenerateToken(applicationUser);
            var response = new LoginResponseDto
            {
                AccessToken = token
            }; 
            return new Response<LoginResponseDto>(response, "Usuario autenticado correctamente");       
        }

        public async Task<Response<bool>> RegisterUser(RegisterUserDto registerUser)
        {
            var applicationUser = new ApplicationUser
            {
                UserName = registerUser.UserName,
                Email = registerUser.Email
            };
                
            var response = await _userManager.CreateAsync(applicationUser, registerUser.Password!);
            if (response.Succeeded)
            {
                return new Response<bool>(response.Succeeded, "Usuario registrado correctamente");
            }
            throw new UserInvalidException(string.Join("\n",response.Errors.Select(e => e.Description)));

        }
    }
}