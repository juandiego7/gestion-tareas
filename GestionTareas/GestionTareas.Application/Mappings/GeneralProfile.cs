﻿using AutoMapper;
using GestionTareas.Application.DTOs;
using GestionTareas.Domain.Entities;

namespace GestionTareas.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile() {
            CreateMap<ApplicationUser, ApplicationUserDto>().ReverseMap();
            CreateMap<TaskEntity, TaskDto>().ReverseMap();
        }
    }
}
