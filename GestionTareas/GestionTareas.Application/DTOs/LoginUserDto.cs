using System.ComponentModel.DataAnnotations;

namespace GestionTareas.Application.DTOs
{
    public class LoginUserDto
    {
        [Required(ErrorMessage ="El correo es requerido.")]
        [EmailAddress(ErrorMessage = "El correo no es una dirección de correo electrónico válida.")]
        [DataType(DataType.EmailAddress)]
        public string? Email { get; set; }
        [Required(ErrorMessage ="La contraseña es requerida.")]
        [DataType(DataType.Password)]    
        public string? Password { get; set; }
    }
}