using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionTareas.Application.DTOs
{
    public class LoginResponseDto
    {
        public string? AccessToken { get; set; }
    }
}