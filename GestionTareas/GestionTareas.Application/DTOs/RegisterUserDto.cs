using System.ComponentModel.DataAnnotations;

namespace GestionTareas.Application.DTOs
{
    public class RegisterUserDto
    {
        public string? UserName { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "El correo no es una dirección de correo electrónico válida.")]
        public string? Email { get; set; }
        [DataType(DataType.Password)]
        [MinLength(6,ErrorMessage = "La contraseña debe tener una longitud mínima de 6.")]
        public string? Password { get; set; }
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Las contraseñas no coinciden.")]
        public string? ConfirmPassword { get; set; }
    }
}