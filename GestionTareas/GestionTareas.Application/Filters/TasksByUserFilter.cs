﻿using Ardalis.Specification;
using GestionTareas.Domain.Entities;

namespace GestionTareas.Application.Filters
{
    public class TasksByUserFilter : Specification<TaskEntity>
    {
        public TasksByUserFilter(string userId)
        {
            if (!string.IsNullOrEmpty(userId))
            {
                Query.Where(task => task.UserId == userId);
            }
        }
    }
}
