using GestionTareas.Application.DTOs;
using GestionTareas.Application.Wrappers;
using GestionTareas.Domain.Entities;

namespace GestionTareas.Application.Interfaces.Services
{
    public interface IAuthService
    {
        Task<Response<bool>> RegisterUser(RegisterUserDto registerUser);
        Task<Response<LoginResponseDto>> Login(LoginUserDto loginUser);
        string GenerateToken(ApplicationUser loginUser);

    }
}
