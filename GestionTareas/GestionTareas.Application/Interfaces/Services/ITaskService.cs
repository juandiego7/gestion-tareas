﻿using GestionTareas.Application.DTOs;
using GestionTareas.Application.Wrappers;

namespace GestionTareas.Application.Interfaces.Services
{
    public interface ITaskService
    {
        Task<Response<IEnumerable<TaskDto>>> GetTasksByUserAsync(string userId);
        Task<Response<TaskDto>> Add(TaskDto task);
        Task<Response<bool>> Update(TaskDto task);
        Task<Response<bool>> Delete(int id);

    }
}
