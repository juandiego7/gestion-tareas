﻿using GestionTareas.Domain.Entities;

namespace GestionTareas.Application.Interfaces.Repositories
{
    public interface ITaskRepository : IRepository<TaskEntity>
    {
    }
}
