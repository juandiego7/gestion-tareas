﻿using Ardalis.Specification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionTareas.Application.Interfaces.Repositories
{
    public interface IRepository<T> : IRepositoryBase<T>,IReadRepositoryBase<T> where T : class
    {

    }
}
